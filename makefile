# ----------------------------------- #
# makefile edited by Mateusz Debowski #
# ----------------------------------- #

SRC_PATH  := src/
OBJ_PATH  := obj/
EXE_PATH  := bin/
LIB_PATH  := lib/
INC_DIRS  := include/

CPP_FILES := $(wildcard $(SRC_PATH)*.c)
OBJ_FILES := $(addprefix $(OBJ_PATH),$(notdir $(CPP_FILES:.c=.o)))

LIBS      := -lpthread
L_FLAGS   := -L./$(LIB_PATH) $(LIBS)
C_FLAGS   := -I./$(INC_DIRS) -g -Wall -pedantic 

EXEC      := prog

$(EXE_PATH)$(EXEC): $(OBJ_FILES)
	$(CC) $(L_FLAGS) -o $@ $^

$(OBJ_PATH)%.o: src/%.c
	$(CC) $(C_FLAGS) -c -o $@ $<

clean:
	rm -f $(OBJ_FILES)
	rm -f $(EXE_PATH)$(EXEC)

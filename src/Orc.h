#ifndef ORC_H
#define ORC_H

#include "IEnemy.h"

struct Orc {
	struct IEnemy *super; // inherit from IEnemy interface
	void (*method)(struct Orc *self);
};

// constructor
struct Orc *Orc_constructor();

// destructor
void Orc_destructor(struct Orc *self);

#endif//ORC_H
#ifndef INTERFACE_H
#define INTERFACE_H

#include "stdbool.h"

struct IEnemy {
	void (*method)(struct IEnemy *self);
};

// constructor
struct IEnemy *IEnemy_constructor();

// destructor
_Bool IEnemy_destructor(struct IEnemy *self);

#endif//INTERFACE_H
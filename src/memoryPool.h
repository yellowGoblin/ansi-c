#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stdint.h"

struct memoryPool {
	size_t free;
	size_t used;
	union {
		void *memory;
		uint8_t *memArr;
	};
};

struct memoryPool *createMemoryPool(size_t size) {
	struct memoryPool *retPool = NULL;

	if(0 < (signed)size) {
		retPool = (struct memoryPool*)malloc(sizeof(struct memoryPool));
		if(retPool) {
			retPool->memory = malloc(size);
			if(retPool->memory) {
				retPool->free = size;
				retPool->used = 0;
			} else {
				free(retPool);
				retPool = NULL;
			}
		}
	}

	return retPool;
}

int destroyMemoryPool(struct memoryPool *self) {
	int retVal = -1;

	if(self) {
		free(self->memory);
		self->memory = NULL;

		free(self);
		self = NULL;

		retVal = 0;
	}

	return retVal;
}

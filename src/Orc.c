#include "Orc.h"

#include "malloc.h"

static void method(struct Orc *self) {
	printf("ORC GROARS\n");
}

// constructor
struct Orc *Orc_constructor() {
	struct Orc *ret;
	ret = malloc(sizeof(struct Orc));
	ret->super = IEnemy_constructor();
	
	ret->method = method;
	return ret;
}

// destructor
void Orc_destructor(struct Orc *self) {
	IEnemy_destructor(self->super);
	free(self);
	self = NULL;
}
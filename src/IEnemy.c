#include "IEnemy.h"

#include "malloc.h"

static void method(struct IEnemy *self) {
	printf("FROM METHOD\n");
}

// constructor
struct IEnemy *IEnemy_constructor() {
	struct IEnemy *retVal = malloc(sizeof(struct IEnemy));
	if(NULL != retVal) {
		retVal->method = method;
	}
	
	return retVal;
}

// destructor
_Bool IEnemy_destructor(struct IEnemy *self) {
	_Bool retVal = false;

	if(NULL != self) { 
		free(self);
		self = NULL;

		retVal = true;
	}

	return retVal;
}

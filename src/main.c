// #include "systemCallsStub.h" // stub system calls (comment this line to disable)

#include "IEnemy.h"
#include "Orc.h"
#include "memoryPool.h"

#include "malloc.h"
#include "stdbool.h"
#include "string.h"
#include "stdint.h"
#include "math.h"

#define LOG_ERR(...) do { fprintf(stderr, __VA_ARGS__); } while(0)
#define LOG_MSG(...) do { printf(__VA_ARGS__); } while(0)

// typedef enum ExitStatus {
//     EXIT_SUCCESS = 0,
//     EXIT_FAILED = 1,
// } ExitStatus;

// static bool isBigEndian() {
//     const unsigned int a = 1;
//     char *b = ((char*)&a);
//     LOG_MSG("ENDIAN: %d\n", !*b);
//     return !*b;
// }

// struct packet {
//     union {
//         uint8_t arr[4];
//         uint32_t noArr;
//     };
//     uint8_t *pointer;
// };

// void *(*getPointer)(void *arg);

// void *myGetPointer(void *arg) { return arg; }

typedef union {
    uint8_t uint8;
    uint32_t uint32;
} unia_europejska;

void poweradder(int *val) { *val = *val + 1; }


struct PowerClassContext { // in cpp
    int privVariable;
    int anotherPrivVar;
};

struct powerClass { // in header
    struct PowerClassContext ctx;
    void (*methodPtr)();
    int (*anotherMethodPrt)();
};

struct powerClass *constructPowerClass(/* constructor parameters */) {
    struct powerClass *ret = NULL;
    ret = malloc(sizeof(struct powerClass));
    // test for errors here
    // ret->methodPtr = myMethod;
    // ret->anotherMethodPtr = myAnotherMethod;
    return ret;
}

int main(int argc, char **argv) {
    // // exit status
    // ExitStatus exitStatus;

    // // declarations
    // struct IEnemy *interf;

    // // construct
    // interf = IEnemy_constructor();

    // // check
    // if(NULL != interf) {
    //     interf->method(interf);

    //     exitStatus = EXIT_SUCCESS;
    // } else {
    //     LOG_ERR("NULL_INTERFACE");

    //     exitStatus = EXIT_FAILED;
    // }
   
    // // destruct
    // IEnemy_destructor(interf);

    // isBigEndian();

    // char *a = "HALOO!";
    // char b[] = "HALOO!";
    // b[0] = 'L';
    
    // char c[strlen(a)+1];
    // strcpy(c, a);

    // printf("%s size: %d, %d \n", a, sizeof(a), strlen(a));
    // printf("%s size: %d, %d \n", b, sizeof(b), strlen(b));
    // printf("%s size: %d, %d \n", c, sizeof(c), strlen(c));

    // int a = strlen("HAHA");
    // printf("a: %d\n", a);

    // unsigned char val = 0xFF;
    // printf("val: %d\n", val);

    // getPointer = myGetPointer;

    // struct packet myPacket;
    // myPacket.noArr = 0x0F0F0F0F;
    // uint8_t *pointer0 = (uint8_t*)&myPacket.noArr;

    // myPacket.pointer = malloc(sizeof(myPacket.pointer)); 
    // *(myPacket.pointer) = 0xFF;

    // LOG_MSG("struct size in bytes: %d\n", sizeof(struct packet));
    // LOG_MSG("val arr: %d\n", myPacket.arr[1]);
    // LOG_MSG("val noArr: %lu\n", (unsigned long int)myPacket.noArr);

    // LOG_MSG("pointer0 val: %d\n", *(pointer0));

    // LOG_MSG("another pointer in packet val: %d\n", *(myPacket.pointer));

    // struct packet *packetPointer;
    // packetPointer = myGetPointer(&myPacket);
    // LOG_MSG("pointer to pocket val arr: %d\n", ((struct packet*)packetPointer)->arr[1]);

    // free(myPacket.pointer);
    // myPacket.pointer = NULL;

    // long i = -1;
    // unsigned char oko;

    // oko = -128;
    // LOG_MSG("oko = %d\n", oko);

    // if (i < sizeof(i)) {
    //     LOG_MSG("OK\n");
    // }
    // else {
    //     LOG_MSG("i in if = %lu\n", i);
    //     LOG_ERR("error\n");
    // }

    struct memoryPool *memPool = createMemoryPool(4);
    LOG_MSG("memPool address: 0x%X\n", (unsigned int)memPool);  // 0 - bad address, otherwise valid address

    //LOG_MSG("free: %d\n", memPool->free);
    //uint8_t *ptr = (uint8_t*)(memPool->memory);
    //ptr[0] = 255;
    //ptr[0] = 0x00;
    //LOG_MSG("ptr: %d, sizeof: %d\n", ptr[0], memPool->free);
    
    memset(memPool->memory, 0x010203aa, 4);

    LOG_MSG("memarr: %d\n", (int)memPool->memArr[0]);

    // LOG_MSG

    //LOG_MSG("mempool mem addr: 0x%X\n", (unsigned int)memPool->memory);
    //LOG_MSG("ptr[0] addr: 0x%X\n", (unsigned int)&ptr[0]);

    int result = destroyMemoryPool(memPool);
    LOG_MSG("destroy result: %d\n", result); // 0 - deleted, otherwise not



    unia_europejska UE;
    UE.uint32 = 0x00000FFF;

    LOG_MSG("32 addr 0x%X, VAL= %d\n", (unsigned int)&(UE.uint32), UE.uint32);
    LOG_MSG("8 addr 0x%X, VAL= %d\n", (unsigned int)&(UE.uint8)+1, *(&(UE.uint8)+1));


    int *pptr[10] = { NULL }; // array of pointers
    pptr[0] = malloc(sizeof(int));
    *pptr[0] = 1; 
    pptr[2] = malloc(sizeof(int));
    *pptr[2] = 10; 
    LOG_MSG("pptr size: %d, pptr[0]: %d, %d\n", sizeof(pptr), *pptr[0], *pptr[2]);

    int **pointertoarrofpointers = NULL; // pointer to array of pointers
    pointertoarrofpointers = pptr;

    LOG_MSG("ptrtoarrofptrs: %d\n", *pointertoarrofpointers[2]);

    free(pptr[0]);
    free(pptr[2]);


    int a = 2;
    poweradder(&a);
    int *aptr = &a;


    LOG_MSG("a: %d, %d\n", a, *aptr);

#define MAGIC 5
    unsigned char **dptr;
    dptr = (unsigned char **)malloc(sizeof(unsigned char*) * MAGIC);
    dptr[0] = malloc(sizeof(char));
    *dptr[0] = 255;

    LOG_MSG("dptr val: %d\n", *dptr[0]);

    free(dptr[0]);
    free(dptr);

    return 0;

    // return exitStatus;
}
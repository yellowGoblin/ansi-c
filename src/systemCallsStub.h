#ifndef STUBS_H
#define STUBS_H

#include "string.h"
#include "malloc.h"

#define strlen strlenStub
#define malloc myMalloc

size_t strlenStub(const char *str); // stub strlen
void *myMalloc(size_t size);        // stub malloc

#endif//STUBS_H